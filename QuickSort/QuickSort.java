import java.util.*;

class QSort{
	
	private int[] array;
	private int mSize;
	private int nItems;
	
	public QSort(int mSize){
		this.mSize = mSize;
		array = new int[mSize];
		nItems = 0;
	}
	
	public void insert(int item){
		if(isFull()){
			System.err.println("ERROR: Array is full");
		}else{
			array[nItems++] = item;
		}
	}
	
	public void sort(){
		qsort(0, nItems-1);
	}
	
	public void qsort(int lBound, int uBound){
		
		if(uBound-lBound <= 0){
			return;
		}
		setMedian(lBound, (lBound+uBound)/2, uBound);
		int partition = partitionIt(lBound, uBound, uBound);
		qsort(lBound, partition-1);
		qsort(partition, uBound);
	}
	
	public int partitionIt(int lBound, int uBound, int pivot){
		
		int lft_ptr = lBound-1;
		int rht_ptr = uBound;
		
		while(true){
			while(array[++lft_ptr] < array[pivot]);
			while(array[--rht_ptr] > array[pivot] && rht_ptr > 0);
			if(lft_ptr >= rht_ptr){
				break;
			}else{
				swap(lft_ptr, rht_ptr);
			}
		}
		
		swap(pivot, lft_ptr);
		
		return lft_ptr;
	}
	
	public void setMedian(int lptr, int mptr, int uptr){
		if(array[lptr] > array[mptr] && array[lptr] < array[uptr] || array[lptr] < array[mptr] && array[lptr] > array[uptr]){
			swap(lptr, uptr);
		}else if(array[mptr] > array[lptr] && array[mptr] < array[uptr] || array[mptr] < array[lptr] && array[mptr] > array[uptr]){
			swap(mptr, uptr);
		}
	}
	
	public void swap(int index1, int index2){
		int temp = array[index1];
		array[index1] = array[index2];
		array[index2] = temp;
	}
	
	public boolean isEmpty(){
		return (nItems == 0);
	}
	
	public boolean isFull(){
		return (nItems == mSize);
	}
	
	public void display(){
		if(isEmpty()){
			System.err.println("ERROR: Array is empty");
		}else{
			for(int i=0; i<nItems; i++){
				System.out.print(array[i] + " ");
			}
			System.out.println("");
		}
	}
	
}

public class QuickSort{
	public static void main(String[] args){
		QSort qsort = new QSort(10);
		qsort.insert(71);
		qsort.insert(22);
		qsort.insert(34);
		qsort.insert(52);
		qsort.insert(61);
		qsort.insert(18);
		qsort.insert(12);
		qsort.insert(73);
		qsort.insert(32);
		qsort.insert(75);
		qsort.insert(2);
		qsort.display();
		
		qsort.sort();
		qsort.display();
	}
}