class SSort{
	
	private int[] array;
	private int index;
	private int mSize;
	
	public SSort(int mSize){
		this.mSize = mSize;
		array = new int[mSize];
		index = 0;
	}
	
	public void insert(int val){
		if(isFull()){
			System.out.println("Array is full!");
		}else{
			array[index++] = val;
		}
	}
	
	public void swap(int i1, int i2){
		if(i1 < 0 || i1 > index || i2 < 0 || i2 > index){
			System.out.println("Error! Index out of bound");
		}else{
			int tmp = array[i1];
			array[i1] = array[i2];
			array[i2] = tmp;
		}
	}
	
	public void sort(){
		int min, min_index;
		for(int i=0; i<index; i++){
			min = array[i];
			min_index = i;
			for(int j=i; j<index; j++){
				if(min > array[j]){
					min = array[j];
					min_index = j;
				}
			}
			swap(i,min_index);
		}
	}
	
	public boolean isEmpty(){
		return (index == 0);
	}
	
	public boolean isFull(){
		return (index == mSize-1);
	}
	
	public void display(){
		for(int i=0; i<index; i++){
			System.out.print(array[i] + " ");
		}
		System.out.print("\n");
	}
	
}

public class SelectionSort{
	public static void main(String[] args){
		SSort ssort = new SSort(10);
		ssort.insert(12);
		ssort.insert(4);
		ssort.insert(6);
		ssort.insert(2);
		ssort.insert(9);
		ssort.insert(1);
		ssort.insert(2);
		ssort.insert(3);
		ssort.display();
		ssort.sort();
		ssort.display();
	}
}