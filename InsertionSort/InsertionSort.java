class ISort{
	
	private int[] array;
	private int mSize;
	private int nItems;
	
	public ISort(int mSize){
		this.mSize = mSize;
		array = new int[mSize];
		nItems=0;
	}
	
	public void insert(int val){
		if(isFull()){
			System.err.println("Error: Array is full!");
		}else{
			array[nItems] = val;
			nItems++;
		}
	}
	
	public void sort(){
		for(int i=1; i<nItems; i++){
			int tmp = array[i];
			int pos = i;
			while(pos > 0 && tmp < array[pos-1]){
				array[pos] = array[pos-1];
				pos--;
			}
			array[pos] = tmp;
		}
	}
	
	public boolean isEmpty(){
		return (nItems == 0);
	}
	
	public boolean isFull(){
		return (nItems == mSize);
	}
	
	public void display(){
		if(isEmpty()){
			System.err.println("Error: Array is empty!");
		}else{
			for(int i=0; i<nItems; i++){
				System.out.print(array[i] + " ");
			}
			System.out.println("");
		}
	}
}

public class InsertionSort{
	public static void main(String[] args){
		ISort iSort = new ISort(10);
		iSort.insert(6);
		iSort.insert(2);
		iSort.insert(7);
		iSort.insert(5);
		iSort.insert(10);
		iSort.insert(12);
		iSort.insert(2);
		iSort.insert(1);
		iSort.display();
		
		iSort.sort();
		iSort.display();
	}
}