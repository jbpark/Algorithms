import java.util.*;
 
class SolveFactorial{
	
	public int solve(int n){
		
		if(n==1){
			return 1;
		}
		
		return n*solve(n-1);
	}
	
}

public class Factorial{
	public static void main(String[] args){
		Scanner in = new Scanner(System.in);
		SolveFactorial factorial = new SolveFactorial();
		while(true){
			System.out.println("Enter a number:");
			System.out.println("Factorial: " + factorial.solve(in.nextInt()));
		}
	}
}