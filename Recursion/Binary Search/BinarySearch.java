class BSearch{
	
	private int[] array;
	private int mSize;
	private int nItems;
	
	public BSearch(int mSize){
		this.mSize = mSize;
		array = new int[mSize];
		nItems = 0;
	}
	
	public void insert(int item){
		if(isFull()){
			System.err.println("Error: Array is full!");
		}else{
			array[nItems] = item;
			nItems++;
		}
	}
	
	public int search(int item, int l_index, int u_index){
		
		int index = (l_index + u_index)/2;
		
		if(l_index == u_index && array[index] != item){
			return -1;
		}else if(array[index] == item){
			return (index+1);
		}else if(array[index] > item){
			return search(item, l_index, index-1);
		}else{
			return search(item, index+1, u_index);
		}
	}
	
	public void insertion_sort(){
		for(int i=1; i<nItems; i++){
			int tmp = array[i];
			int pos = i;
			while(pos > 0 && array[pos-1] > tmp){
				array[pos] = array[pos-1];
				pos--;
			}
			array[pos] = tmp;
		}
	}
	
	public void selection_sort(){
		int min;
		int min_index;
		
		for(int i=0; i<nItems; i++){
			min=array[i];
			min_index = i;
			for(int j=i; j<nItems; j++){
				if(min > array[j]){
					min = array[j];
					min_index = j;
				}
			}
			swap(i,min_index);
		}
	}
	
	public void swap(int i1, int i2){
		int tmp = array[i1];
		array[i1] = array[i2];
		array[i2] = tmp;
	}
	
	public boolean isEmpty(){
		return (nItems == 0);
	}
	
	public boolean isFull(){
		return (nItems == mSize);
	}
	
	public void display(){
		for(int i=0; i<nItems; i++){
			System.out.print(array[i] + " ");
		}
		System.out.println("");
	}
	
	public int getnItems(){
		return nItems;
	}
}

public class BinarySearch{
	
	public static void main(String[] args){
		
		BSearch bsearch = new BSearch(10);
		bsearch.insert(11);
		bsearch.insert(22);
		bsearch.insert(34);
		bsearch.insert(52);
		bsearch.insert(61);
		bsearch.insert(18);
		bsearch.insert(12);
		bsearch.insert(73);
		bsearch.insert(32);
		bsearch.insert(75);
		bsearch.insert(2);
		
		bsearch.display();
		bsearch.insertion_sort();
		bsearch.selection_sort();
		bsearch.display();
		
		int index = bsearch.search(52, 0, bsearch.getnItems()-1);
		
		switch (index){
			case -1:
				System.out.println("Item does not exist");
				break;
			default:
				System.out.println(index);
		}
	}
}
