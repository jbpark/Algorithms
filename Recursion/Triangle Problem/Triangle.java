import java.util.*;

class TProblem{
	
	public int solve(int n){
		
		if(n==1){
			return 1;
		}else{
			return n+solve(n-1);
		}
	}
}

public class Triangle{
	public static void main(String[] args){
		
		Scanner in = new Scanner(System.in);
		
		TProblem triangle = new TProblem();
		System.out.println("Enter a number:");
		System.out.println("Triangle: " + triangle.solve(in.nextInt()));
	}
}