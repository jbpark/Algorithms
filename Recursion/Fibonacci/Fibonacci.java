import java.util.*;

class Fib_Solver{
	
	public int solve(int n){
		if(n==1){
			return 1;
		}else if (n==2){
			return 1;
		}
		return (solve(n-1) + solve(n-2));
	}
	
}

public class Fibonacci{
	public static void main(String[] args){
		Scanner in = new Scanner(System.in);
		Fib_Solver fib = new Fib_Solver();
		while(true){
			System.out.println("Enter a number:");
			System.out.println("Fibonacci: " + fib.solve(in.nextInt()));
		}
	}
}