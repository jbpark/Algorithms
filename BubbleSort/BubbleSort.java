class BSort{
	
	private int[] array;
	private int mSize;
	private int index;
	
	public BSort(int mSize){
		this.mSize = mSize;
		array = new int[mSize];
		index=0;
	}
	
	public void insert(int val){
		if(isFull()){
			System.out.println("Array is full!");
		}else{
			array[index++] = val;
		}
	}
	
	public void swap(int i1, int i2){
		if(i1 < 0 || i1 > index || i2 < 0 || i2 > index){
			System.out.println("Error! Index out of bound");
		}else{
			int tmp = array[i1];
			array[i1] = array[i2];
			array[i2] = tmp;
		}
	}
	
	public void sort(){
		int pos = index-1;
		for(int i=0; i<index; i++){
			for(int j=0; j<pos; j++){
				if(array[j] > array[j+1]){
					swap(j,j+1);
				}
			}
			pos--;
		}
	}
	
	public boolean isEmpty(){
		return (index == 0);
	}
	
	public boolean isFull(){
		return (index == mSize-1);
	}
	
	public void display(){
		for(int i=0; i<index; i++){
			System.out.print(array[i] + " ");
		}
		System.out.print("\n");
	}
}

public class BubbleSort{
	public static void main(String[] args){
		BSort bsort = new BSort(10);
		bsort.insert(12);
		bsort.insert(4);
		bsort.insert(6);
		bsort.insert(2);
		bsort.insert(9);
		bsort.insert(1);
		bsort.insert(2);
		bsort.insert(3);
		bsort.display();
		bsort.sort();
		bsort.display();
		
	}
}