class MSort{
	
	private int[] list;
	private int mSize;
	private int nItems;
	
	public MSort(int mSize){
		this.mSize = mSize;
		list = new int[mSize];
		nItems=0;
	}
	
	public void insert(int item){
		if(isFull()){
			System.err.println("ERROR: List is full");
		}else{
			list[nItems++] = item;
		}
	}
	
	public void sort(){
		int[] workspace = new int[nItems];
		msort(workspace, 0, nItems-1);
	}
	
	public void msort(int[] workspace, int l_bound, int u_bound){
		
		if(l_bound == u_bound){
			return;
		}
		
		int m_bound = (l_bound + u_bound)/2;
		msort(workspace, l_bound, m_bound);
		msort(workspace, m_bound+1, u_bound);
		merge(workspace, l_bound, m_bound, u_bound);
	}
	
	public void merge(int[] workspace, int l_bound, int m_bound, int u_bound){
		
		int j=0;
		int index1 = l_bound;
		int index2 = m_bound+1;
		
		while(index1 <= m_bound && index2 <= u_bound){
			if(list[index1] > list[index2]){
				workspace[j++] = list[index2++];
			}else{
				workspace[j++] = list[index1++];
			}
		}
		
		while(index1 > m_bound && index2 <= u_bound){
			workspace[j++] = list[index2++];
		}
		
		while(index2 > u_bound && index1 <= m_bound){
			workspace[j++] = list[index1++];
		}
		
		for(int i=0; i<j; i++){
			list[l_bound+i] = workspace[i];
		}	
	}
	
	public boolean isEmpty(){
		return (nItems == 0);
	}
	
	public boolean isFull(){
		return (nItems == mSize);
	}
	
	public void display(){
		if(isEmpty()){
			System.err.println("ERROR: List is empty");
		}else{
			for(int i=0; i<nItems; i++){
				System.out.print(list[i] + " ");
			}
			System.out.println("");
		}
	}
}

public class MergeSort{
	public static void main(String[] args){
		MSort msort = new MSort(10);
		msort.insert(71);
		msort.insert(22);
		msort.insert(34);
		msort.insert(52);
		msort.insert(61);
		msort.insert(18);
		msort.insert(12);
		msort.insert(73);
		msort.insert(32);
		msort.insert(75);
		msort.insert(2);
		msort.display();
		
		msort.sort();
		msort.display();
	}
}